let num = prompt("Enter a number:");
let sum = 0;

for (let i = 0; i < num.length; i++) {
  sum += parseInt(num.charAt(i));
}

console.log("The sum of the digits of " + num + " is " + sum);
